<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Impl;

use CodingPaws\Layer4\Base\DuplexClient;
use CodingPaws\Layer4\Base\HasSocketInit;

final class TCPSocketClient extends DuplexClient
{
  use HasSocketInit;

  private bool $blocking = false;

  public function send(string $data): void
  {
    socket_write($this->socket, $data, strlen($data));
  }

  public function read(int $size = 1024): ?string
  {
    $data = null;
    $flags = $this->blocking ? MSG_WAITALL : MSG_DONTWAIT;
    socket_recv($this->socket, $data, $size, $flags);

    return $data;
  }

  public function blocking(bool $blocking): void
  {
    $this->blocking = $blocking;
  }

  public function isConnected(): bool
  {
    $address = null;
    return $this->socket && socket_getpeername($this->socket, $address);
  }

  public function type(): int
  {
    return SOCK_STREAM;
  }

  public function protocol(): int
  {
    return SOL_TCP;
  }
}
