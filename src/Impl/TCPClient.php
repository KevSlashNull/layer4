<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Impl;

use CodingPaws\Layer4\Base\DuplexClient;
use CodingPaws\Layer4\Exceptions\ConnectionInitException;

final class TCPClient extends DuplexClient
{
  /**
   * @var resource|false
   */
  private $socket;

  public function init(): void
  {
    $error_code = 0;
    $error_message = '';
    $this->socket = fsockopen(
      $this->address,
      $this->port,
      $error_code,
      $error_message,
      timeout: 10
    );

    if ($error_message || $error_code) {
      fclose($this->socket);
      throw new ConnectionInitException(
        "Couldn't connect to RPC service: ${error_message}",
        $error_code
      );
    }
  }

  public function send(string $data): void
  {
    fwrite($this->socket, $data, strlen($data));
  }

  public function read(int $size = 1024): ?string
  {
    $result = fread($this->socket, $size);

    if (is_bool($result)) {
      return null;
    }

    return $result;
  }

  public function isConnected(): bool
  {
    if (!$this->socket) {
      return false;
    }

    $status = socket_get_status($this->socket);

    return !$status['timed_out'] || !$status['eof'];
  }

  public function close(): void
  {
    fclose($this->socket);
    $this->socket = null;
  }
}
