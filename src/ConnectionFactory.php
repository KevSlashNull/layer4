<?php

declare(strict_types=1);

namespace CodingPaws\Layer4;

use CodingPaws\Layer4\Base\DuplexClient;
use CodingPaws\Layer4\Base\SimplexClient;
use CodingPaws\Layer4\Impl\TCPClient;
use CodingPaws\Layer4\Impl\TCPSocketClient;
use CodingPaws\Layer4\Impl\UDPSocketClient;

final class ConnectionFactory
{
  public static function tcpSocket(
    string $address,
    int $port,
    bool $blocking = false
  ): DuplexClient {
    $connection = new TCPSocketClient($address, $port);
    $connection->blocking($blocking);
    return $connection;
  }

  public static function tcp(string $address, int $port): DuplexClient
  {
    return new TCPClient($address, $port);
  }

  public static function udp(string $address, int $port): SimplexClient
  {
    return new UDPSocketClient($address, $port);
  }
}
