<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Base;

interface Connection
{
  /**
   * Check whether the connection is
   * currently connceted to a remote.
   */
  public function isConnected(): bool;

  /**
   * Close the connection if it is
   * connected to a remote.
   *
   * @see Connection::isConnected
   */
  public function disconnect(): void;
}
