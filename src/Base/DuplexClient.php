<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Base;

abstract class DuplexClient extends SimplexClient implements Connection
{
  /**
   * Read at most $size bytes from the remote server
   */
  abstract public function read(int $size = 1024): ?string;

  public function disconnect(): void
  {
    if ($this->isConnected()) {
      $this->close();
    }
  }
}
