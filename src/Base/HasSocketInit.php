<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Base;

use CodingPaws\Layer4\Exceptions\ConnectionInitException;
use Socket;

trait HasSocketInit
{
  protected ?Socket $socket;

  public function __destruct()
  {
    if ($this->socket) {
      socket_close($this->socket);
    }
  }

  public function init(): void
  {
    $this->socket = socket_create(AF_INET, $this->type(), $this->protocol());

    if (!$this->socket) {
      throw new ConnectionInitException('Could not create socket.');
    }

    $success = socket_connect($this->socket, $this->address, $this->port);

    if (!$success) {
      throw new ConnectionInitException(
        "Couldn't connect to {$this->address}:{$this->port}"
      );
    }
  }

  public function close(): void
  {
    if ($this->socket) {
      socket_close($this->socket);
      $this->socket = null;
    }
  }

  abstract public function type(): int;
  abstract public function protocol(): int;
}
