<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Base;

interface Client extends Closeable
{
  /**
   * The remote address of the server which the
   * client is trying to interact with.
   */
  public function address(): string;

  /**
   * The remote port of the server which the
   * client is trying to interact with.
   */
  public function port(): int;

  /**
   * Send a string to the remote server.
   *
   * It is up to the implementation to split
   * the data up into multiple datagrams.
   */
  public function send(string $data): void;
}
