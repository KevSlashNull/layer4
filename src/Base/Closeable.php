<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Base;

interface Closeable
{
  /**
   * Close a resource held by the object, or
   * the object itself if it is a resource.
   */
  public function close(): void;
}
