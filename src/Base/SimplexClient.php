<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Base;

abstract class SimplexClient implements Client
{
  protected string $address;
  protected int $port;

  public function __construct(string $address, int $port)
  {
    $this->address = $address;
    $this->port = $port;
    $this->init();
  }

  public function __destruct()
  {
    $this->attemptClose();
  }

  public function address(): string
  {
    return $this->address;
  }

  public function port(): int
  {
    return $this->port;
  }

  /**
   * Initialize the client.
   */
  abstract protected function init(): void;

  private function attemptClose(): void
  {
    try {
      $this->close();
    } catch (\Throwable $th) {
      // The user could've already closed this
      return;
    }
  }
}
