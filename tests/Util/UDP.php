<?php

namespace Tests\Util;

use Socket;

final class UDP
{
  public static function createServer(): Socket
  {
    $udp = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    socket_bind($udp, "127.0.0.1", 12346);

    return $udp;
  }

  public static function closeServer(Socket $udp)
  {
    socket_close($udp);
  }
}
