<?php

namespace Tests\Util;

use Socket;

final class TCP
{
  public static function createServer(): Socket
  {
    $tcp = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    socket_bind($tcp, "127.0.0.1", 12345);
    socket_listen($tcp, 1);
    socket_set_option($tcp, SOL_SOCKET, SO_LINGER, [
      'l_linger' => 0,
      'l_onoff' => 1,
    ]);

    return $tcp;
  }

  public static function closeServer(Socket $tcp)
  {
    socket_close($tcp);
  }
}
