<?php

use CodingPaws\Layer4\ConnectionFactory;
use CodingPaws\Layer4\Impl\TCPClient;
use Tests\Util\TCP;

describe(TCPClient::class, function () {
  subject(fn () => ConnectionFactory::tcp("127.0.0.1", 12345));

  before(function () {
    $this->server = TCP::createServer();
  });

  after(function () {
    TCP::closeServer($this->server);
  });

  it('is connected by default', function () {
    expect(subject()->isConnected())->toBe(true);
  });

  foreach (['disconnect', 'close'] as $method) {
    describe("#$method", function () use ($method) {
      let('method', $method);

      it('disconnects the client', function () {
        subject()->{$this->method}();

        expect(subject()->isConnected())->toBe(false);
      });
    });
  }

  describe('#send', function () {
    it('sends a message to the server', function () {
      subject()->send('test');

      $data = null;
      $socket = socket_accept($this->server);
      socket_recv($socket, $data, 1024, MSG_DONTWAIT);
      socket_close($socket);

      expect($data)->toBe('test');
    });
  });

  describe('#receive', function () {
    it('receives a message from the server', function () {
      subject();

      $socket = socket_accept($this->server);

      $payload = '{"id": 1, "message": "hello world!"}';
      socket_send($socket, $payload, strlen($payload), 0);

      expect(subject()->read())->toBe($payload);
    });
  });
});
